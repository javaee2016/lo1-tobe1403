package se.miun.dsv.jee.jpa.example.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by tomasoberg on 2016-11-08.
 */

@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

    /**
     * Protected variabels
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String comment;
    private LocalDateTime time;
    private LocalDateTime lastUpdate;

    //-------------------------------------------------------------------------
    // Pekar på user.comments
    //-------------------------------------------------------------------------
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private User user;

    //-------------------------------------------------------------------------
    // Pekar på event.comments
    // Skapar en kolumn att vara FK till det event denna kommentar pekar på:
    //-------------------------------------------------------------------------
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "EVENT_ID")
    private Event event;


    /**
     * Getters
     */
    public String getComment()  {
        return comment;
    }
    public long getId()         {
        return id;
    }
    public LocalDateTime getTime() {
        return time;
    }
    public User getUser() {  return user;}
    public Event getEvent() {
        return event;
    }


    /**
     * Setters
     */
    public void setTime(LocalDateTime time) {
        this.time = time;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void setEvent(Event event) {
        this.event = event;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Constructors
     */
    public Comment() { }
    public Comment(String comment, LocalDateTime time, User user) {
        this.comment = comment;
        this.time = time;
        this.user = user;
        this.lastUpdate = LocalDateTime.now();
    }

}
