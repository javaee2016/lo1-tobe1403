package se.miun.dsv.jee.jpa.example;

import se.miun.dsv.jee.jpa.example.model.*;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.io.*;

import java.net.MalformedURLException;
import java.net.URL;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The class that populates the database. When you have your database setup,
 * running the main method should create and persist 100 watermelons with their
 * seeds using the "WATERMELON" join column in the SEEDS database table.
 *
 */
public class DatabasePopulator {

	public static void main(String[] args) {


        //---------------------------------------------------------------------
        // Skapa en container för alla events
        //---------------------------------------------------------------------
        List<Event> events = new ArrayList();

        //---------------------------------------------------------------------
        // Koppla upp mot adressen:
        // och läs av textfilen rad för rad.
        //---------------------------------------------------------------------
        URL url = null;
        Scanner s;
        try {
            url = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        StringBuilder input = new StringBuilder();
        try {
            assert url != null;
            Scanner scanner = new Scanner(url.openStream());
            while(scanner.hasNextLine()) {
                input.append(scanner.nextLine());
                input.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String userAndEventData = input.toString();


        /*
        //---------------------------------------------------------------------
        // TODO: Läs in från filen events.txt med hjälp av niobiblioteket
        //---------------------------------------------------------------------
        */
        /*
        Path file;
        file = new Paths.get("file:///events.txt");

        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }*/

        //---------------------------------------------------------------------
        // Börjar med användarna:
        // Skala bort texten "Users:\n" först med en substringfunktionen
        // Applicera därefter regexparttern och plocka ut
        // email, för- och efternamn. Lägg in i kollektionen users.
        //---------------------------------------------------------------------
        int userspos    = userAndEventData.indexOf("Users:\n");
        int eventspos   = userAndEventData.indexOf("Events:\n");
        String userspart = userAndEventData.substring(userspos + 7, eventspos);
        String userRegexPattern =
                "(?<fName>([^\\s\\\\]*)) " +
                        "(?<lName>([^\\s\\\\]*))\\s*" +
                        "(?<email>(.*))\\n";
        Pattern usersPattern = Pattern.compile(userRegexPattern);
        Matcher m = usersPattern.matcher(userspart);
        List<User> users = new ArrayList<User>();
        while (m.find()) {
            final User user = new User(m.group("email"), m.group("fName"), m.group("lName"));
            //System.out.println("Creating user: " + user.getFirstName() + " " + user.getLastName());
            users.add(user);
        }


        //---------------------------------------------------------------------
        // Skapar grupper ur varje matchning från indatat i ett event:
        // name, location, description, start, end, users och comments
        // och skapa ett regexpatternobjekt för det också
        //---------------------------------------------------------------------
        String eventsregexpPattern =
                "(?<name>([^,]*)),\\s*" +
                        "(?<location>([^,]*)),\\s*" +
                        "(?<description>([^,]*)),\\s*" +
                        "(?<start>(.*))\\s*-\\s*" +
                        "(?<end>(.*))\\s*,\\s*\\[" +
                        "(?<users>(.*))\\]\\s*" +
                        "(?<comments>\\t.*\\n)?(\n)?";
        Pattern eventsPattern = Pattern.compile(eventsregexpPattern, Pattern.MULTILINE);


        //---------------------------------------------------------------------
        // Skapar Regex för att dela upp kommentarer i email, time, och comment
        //---------------------------------------------------------------------
        String groupOfUsersRegexpPattern = "[0-9a-zA-Z.@]+(,[0-9a-zA-Z.@]+)*";
        Pattern userGroupPattern = Pattern.compile(groupOfUsersRegexpPattern);
        String commentPerUserRegexpPattern =
                "(?<email>(.*[^\\s*]))\\s*\\(" +
                        "(?<time>(.*))\\)\\:\\s*\\\"" +
                        "(?<comment>(.*))\\\"\\n*";

        Pattern commentsPattern = Pattern.compile(commentPerUserRegexpPattern);


        //---------------------------------------------------------------------
        // Skapar en formaterare för att handskas med datum från strängar
        //---------------------------------------------------------------------
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");

        //---------------------------------------------------------------------
        // Plocka ut eventdelen ur strängen med data och splitta upp alla event
        // för varje funnen dubbelnyrad
        //---------------------------------------------------------------------
        String eventspart = userAndEventData.substring(eventspos + 8, userAndEventData.length());
        final String[] allEventParts = eventspart.split("\n\n");

        //---------------------------------------------------------------------
        // Loopa igenom alla eventsubsträngar
        //---------------------------------------------------------------------
        for (String eachEventString : allEventParts) {

            //-----------------------------------------------------------------
            // Kolla efter matchingar efter formulan i eventsPattern (eventsregexpPattern)
            // System.out.println("Matchar mot START"+eachEventString+"STOP");
            //-----------------------------------------------------------------
            Matcher eventMatcher = eventsPattern.matcher(eachEventString);
            if (eventMatcher.find()) {
                //System.out.println("Creating EVENT: " + eventMatcher.group("name"));

                List<User> usersForThisEvent = new ArrayList<>();
                String theUsers = eventMatcher.group("users");
                if (theUsers != null) {
                    Matcher usergroup = userGroupPattern.matcher(theUsers);

                    //---------------------------------------------------------
                    // Se om vi hittar en matchande användare,
                    // lägg i så fall in en användare i en container
                    // (som sedan läggs till till eventet i slutet)
                    //---------------------------------------------------------
                    while (usergroup.find()) {
                        for (User eachUser : users) {
                            System.out.println("Matchar " +eachUser.getEmail()+ " mot " + usergroup.group(0));
                            if (eachUser.getEmail().equals(usergroup.group(0))) {

                                usersForThisEvent.add(eachUser);
                            }
                        }
                    }
                }
                //-------------------------------------------------------------
                // Gå igenom alla kommentarer (uppsplittning först efter tabbar
                // (\t), därefter patternmatchning efter tidigare definierad
                // Patternobjekt för kommentarer.
                //-------------------------------------------------------------
                ArrayList<Comment> commentsForThisEvent = new ArrayList<>();
                final String[] allComments = eachEventString.split("\t");
                for (String eachComment : allComments) {
                    if (eachComment.length() > 0) {

                        Matcher commentMatcher = commentsPattern.matcher(eachComment);
                        while (commentMatcher.find()) {

                            //-------------------------------------------------
                            // Konvertera om sträng till LocalDateTimeobjekt
                            //-------------------------------------------------
                            String timeAsString = commentMatcher.group("time");
                            LocalDateTime dateTime = LocalDateTime.parse(timeAsString, formatter);

                            //-------------------------------------------------
                            // Hitta en användare utifrån email och vid träff,
                            // lägg till kommentaren till en lista med kom-
                            // mentarer tillsammans med tidpunkt och användare
                            //-------------------------------------------------
                            for (User eachUser : users) {
                                if (eachUser.getEmail().equals(commentMatcher.group("email"))) {
                                    final Comment theComment = new Comment(commentMatcher.group("comment"), dateTime, eachUser);
                                    commentsForThisEvent.add(theComment);
                                    System.out.println("Adding comment from: " + theComment.getUser().getFirstName());
                                }
                            }
                        }
                    }
                }


                //-------------------------------------------------------------
                // Skapar event här nedan. Läser först av datum och plockar
                // bort eventuella mellanslag.
                //-------------------------------------------------------------
                String startTimeAsString = eventMatcher.group("start");
                if (startTimeAsString.length() > 14) {
                    startTimeAsString = startTimeAsString.substring(0, 14);
                }

                //-------------------------------------------------------------
                // Formaterra datum efter strängar och skapa LocalDateTime-
                // objekt för start- och sluttid.
                //-------------------------------------------------------------
                LocalDateTime startTime = LocalDateTime.parse(startTimeAsString, formatter);
                String endTimeAsString = eventMatcher.group("end");
                LocalDateTime endTime = LocalDateTime.parse(endTimeAsString, formatter);

                //-------------------------------------------------------------
                // Skapa ett nytt event av alla delar. Lägg till i kollektionen
                // events
                //-------------------------------------------------------------
                Event event = new Event(eventMatcher.group("name"),
                                        eventMatcher.group("location"),
                                        eventMatcher.group("description"),
                                        startTime,
                                        endTime,
                                        commentsForThisEvent,
                                        usersForThisEvent);

                events.add(event);
            } else {
                System.out.println("Data mismatch. Wrong format. ");
            }

        }
        System.out.println("Parsing done!");




        //---------------------------------------------------------------------
        // OBS: Dyr: Skapa bara EntityManagerFactory en gång
        //---------------------------------------------------------------------
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("example");

        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction tx = manager.getTransaction();

        try {
            tx.begin();

            for(Event event : events)
                manager.persist(event);

            tx.commit();

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            manager.close();
        }


        entityManagerFactory.close();

    }
}
