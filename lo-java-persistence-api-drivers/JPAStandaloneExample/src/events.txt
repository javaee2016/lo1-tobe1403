Users:
Örjan Sterner	orjan.sterner@miun.se
Felix Dobslaw	felix.dobslaw@miun.se
Per Ekeroot	per.ekeroot@miun.se
Pär-Ove Forss	par-ove.forss@miun.se
Christoffer Fink	christoffer.fink@miun.se
Fredrik Aletind	fredrik.aletind@miun.se
Robert Jonsson	robert.jonsson@miun.se
Mikael Nilsson	mikael.nilsson@miun.se
Börje Hansson	borje.hansson@miun.se

Events:
City Cleaning, Hamburg, Anual street cleaning in the streets of Lurup, 16/01/02 10:00 - 16/01/02 16:00, [felix.dobslaw@miun.se, per.ekeroot@miun.se]
	orjan.sterner@miun.se (15/10/01 11:32): "I can't join, I have to help my daughter move into her new appartment in Stockholm."
	felix.dobslaw@miun.se (15/10/01 11:36): "OK, but we count on you for the protest in July!"
	christoffer.fink@miun.se (15/10/01 11:42): "Wow, now that housing prices are skyrocketing!"

Refugees Welcome!, Hamburg, Collecting money for UNHCR, 17/04/03 10:00 - 17/04/03 16:30, [christoffer.fink@miun.se, robert.jonsson@miun.se]

Food for the Homeless, Hamburg, Giving out food for the homeless in St. Georg, 16/12/06 10:00 - 16/12/06 16:00, [orjan.sterner@miun.se, par-ove.forss@miun.se, per.ekeroot@miun.se]

Occupation Protest, Moskva, Protest against the imprisoning of Pussy Riots, 17/07/04 11:00 - 17/07/10 23:00, [fredrik.aletind@miun.se]
	christoffer.fink@miun.se (16/10/30 03:12): "I have bought the train tickets now, where do we meet up?"
	per.ekeroot@miun.se (16/10/30 08:02): "I think Pär-Ove said in front of the Kreml."
	felix.dobslaw@miun.se (16/10/30 08:43): "ACK"
	robert.jonsson@miun.se (16/11/01 13:10): "Felix, do you really believe that a topless protest with us makes an impression on the Russian government?"
	felix.dobslaw@miun.se (16/11/01 14:12): "Yes, this will be spread in social-media around the globe."

City Cleaning, Östersund, Anual street cleaning on Frösön, 17/03/15 09:00 - 17/03/15 17:00, [felix.dobslaw@miun.se, per.ekeroot@miun.se]

Refugees Welcome!, Östersund, Collecting money for UNHCR, 17/02/27 08:30 - 17/02/27 12:30, [robert.jonsson@miun.se]
	fredrik.aletind@miun.se (16/10/20 14:55): "Very good idea, Robert! Count me in."

Supper for the Homeless, Östersund, Handing out supper for the homeless in Kyrkparken, 17/12/06 07:00 - 17/12/06 14:00, [felix.dobslaw@miun.se, per.ekeroot@miun.se]

Occupation Protest, Östersund, Protest against the Swedish Government for it's treatment of the Julian Assange trial, 16/12/03 00:00 - 16/12/14 23:00, [felix.dobslaw@miun.se]
	par-ove.forss@miun.se (16/11/01 03:12): "We are state-employees, should we really do this?"
	felix.dobslaw@miun.se (16/11/01 04:43): "He has been isolated in the Ecuadorian ambassy for over 2 years now, we have to support his cause."
	per.ekeroot@miun.se (16/11/01 08:01): "I agree with PO. We might loose our jobs over this."
